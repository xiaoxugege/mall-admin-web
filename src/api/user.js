import request from '@/utils/request'

export function getUserList(data) {
  return request({
    url:'/member/pagedQuery',
    method:'post',
    data:data
  })
}

export function userUpdate(data) {
  return request({
    url:'/member/update',
    method:'post',
    data:data
  })
}

// 商城訂單
export function getShopList(data) {
  return request({
    url:'/order/v2/pagedQuery',
    method:'post',
    data:data
  })
}

// 地址列表
export function getAddressList(data) {
  return request({
    url:'memberAddress/pagedQuery',
    method:'post',
    data:data
  })
}

// 收益列表
export function getEstimateList(data) {
  return request({
    url:'/predict/pagedQuery',
    method:'post',
    data:data
  })
}

// 資金流水
export function getMoneyList(data) {
  return request({
    url:'/userBalanceChange/pagedQuery',
    method:'post',
    data:data
  })
}

// 收益
export function getAccount(data) {
  return request({
    url:'/member/account/'+ data,
    method:'get'
  })
}

// 修改上级ID
export function updateBefId(data) {
  return request({
    url:'/member/updateBefId',
    method:'post',
    data:data
  })
}
import request from '@/utils/request'
//提现接口
export function pageQueryUser(query) {
    return request({
        url: process.env.BASE_API + '/memberWithdrawLog/pagedQuery',
        method: 'post',
        data: query
    })
}
//提现结果导入
export function completeExcel(query) {
    return request({
        url: process.env.BASE_API + '/admin/withdraw/completeExcel',
        method: 'post',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        data: query
    })
}